variable "name" {
  description = "Resource name"
  type        = string
}

variable "location" {
  description = "Azure region to use"
  type        = string
}
