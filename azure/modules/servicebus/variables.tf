variable "location" {
  description = "Azure region to use"
  type        = string
}

variable "resource_group_name" {
  description = "Resource group name"
  type        = string
}

variable "namespace_name" {
  description = "Namespace name"
  type        = string
}

variable "topic_name" {
  description = "Topic name"
  type        = string
}

variable "subscription_name" {
  description = "Subscription name"
  type        = string
}
