output "id" {
  value       = azurerm_servicebus_namespace.sbn.id
  description = "Resource generated id"
}
