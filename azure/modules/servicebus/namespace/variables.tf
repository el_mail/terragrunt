variable "name" {
  description = "Resource name"
  type        = string
}

variable "location" {
  description = "Azure region to use"
  type        = string
}

variable "resource_group_name" {
  description = "Resource group name"
  type        = string
}
