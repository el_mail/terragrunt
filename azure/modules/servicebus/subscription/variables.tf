variable "name" {
  description = "Resource name"
  type        = string
}

variable "topic_id" {
  description = "Topic id"
  type        = string
}
