resource "azurerm_servicebus_subscription" "sbs" {
  name               = var.name
  topic_id           = var.topic_id
  max_delivery_count = 1
}
