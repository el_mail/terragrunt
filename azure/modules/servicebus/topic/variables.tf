variable "name" {
  description = "Resource name"
  type        = string
}

variable "namespace_id" {
  description = "Namespace id"
  type        = string
}
