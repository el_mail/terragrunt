resource "azurerm_servicebus_topic" "sbt" {
  name         = var.name
  namespace_id = var.namespace_id

  enable_partitioning = true
}
