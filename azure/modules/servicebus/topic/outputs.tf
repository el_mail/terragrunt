output "id" {
  value       = azurerm_servicebus_topic.sbt.id
  description = "Resource generated id"
}