module "namespace" {
  source = "./namespace"
  name   = var.namespace_name

  location            = var.location
  resource_group_name = var.resource_group_name
}

module "topic" {
  source = "./topic"

  name         = var.topic_name
  namespace_id = module.namespace.id
}

module "subscription" {
  source = "./subscription"

  name     = var.subscription_name
  topic_id = module.topic.id
}
