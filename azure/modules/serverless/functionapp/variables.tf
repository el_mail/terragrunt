variable "location" {
  description = "Azure region to use"
  type        = string
}

variable "resource_group_name" {
  description = "Resource group name"
  type        = string
}

variable "function_app_name" {
  description = "Function App name"
  type        = string
}

variable "app_service_plan_id" {
  type        = string
}

variable "storage_account_name" {
  type        = string
}

variable "storage_account_primary_access_key" {
  type        = string
}
