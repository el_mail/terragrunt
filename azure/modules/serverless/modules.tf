resource "azurerm_storage_account" "sa" {
  name                     = var.storage_account_name
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "appsp" {
  name                = var.app_service_plan_name
  location            = var.location
  resource_group_name = var.resource_group_name
  kind                = "FunctionApp"

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

module "functionapp" {
  source = "./functionapp"

  function_app_name                  = var.function_app_name
  location                           = var.location
  resource_group_name                = var.resource_group_name
  app_service_plan_id                = azurerm_app_service_plan.appsp.id
  storage_account_name               = azurerm_storage_account.sa.name
  storage_account_primary_access_key = azurerm_storage_account.sa.primary_access_key
}
