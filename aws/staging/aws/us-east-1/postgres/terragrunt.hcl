locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))

  env = local.environment_vars.locals.environment
}

terraform {
  source = "${get_parent_terragrunt_dir()}/modules//postgres"
  # source = "git::git@github.com:gruntwork-io/terragrunt-infrastructure-modules-example.git//mysql?ref=v0.4.0"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name           = "postgres_${local.env}"
  instance_class = "db.t3.micro"

  allocated_storage = 20
  storage_type      = "standard"

  master_username = "postgres"
  # TODO: To avoid storing your DB password in the code, set it as the environment variable TF_VAR_master_password
}
