aws s3 mb s3://terragrunt-adaptive-terraform-state-staging-us-east-1 --region us-east-1

aws s3api put-bucket-versioning --bucket \
terragrunt-adaptive-terraform-state-staging-us-east-1 --versioning-configuration Status=Enabled

export TF_VAR_master_password=changeme!